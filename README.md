# Instruction
``` 
See the README.md file for notes & the examples directory for code examples 
```

# Notes
# I. Javascript: An Introduction
## 1. What is Javascript?
JavaScript is a programming language that allows you to implement complex things on web pages — every time a web page does more than just sit there and display static information for you to look at — displaying timely content updates, or interactive maps, or animated 2D/3D graphics, or scrolling video jukeboxes, etc. — you can bet that JavaScript is probably involved. It is the third layer of the layer cake of standard web technologies.  
- HTML: content layer  
- CSS: presentation layer  
- JavaScript: interactive layer  

## 2. Navigating the Javascript landscape
### Concepts
- Javascript: Conforms to the ever-evolving ECMAScript standard
- ECMAScript 2015: the current cutting-edge specification, previously known as ECMAScript 6 (ES6)
- ECMAScript: Browsers use ECMAScript to interpret Javascript; so from your perspective, they are effectively the same thing. 
- Current Landscape (Spring 2017)
  - ECMAScript 5.1 (2011): current (outdated) standard
  - ECMAScript 2015 (ES6): emerging standard (spotty support); needs a transpiler like Babel to work
  - ECMAScript 2016 (ES7): in development; has no real support

### Libraries
#### jQuery:  
- Library of Javascript functions
- Introduces CSS-like syntax and several visual and UI enhancements
- Simplifies the use of Javascript in websites
- An abstraction of the languages

### Frameworks
AngularJS, React, Vue.js, and so on are front-end application frameworks used to simplify the building of advanced interactive web-based applications.  
Node.js is a Javascript platform build on browser runtime environments, used to run advanced operations and applications on servers and computers using Javascript.

# II. The Basics
## 1. Tools for Javascript development
- Code editor (Atom, Visual Studio, Sublime Text, WebStorm)
- Browsers with developer tools
- Helper tools for debugging, etc.

## 2. How to add Javascript to a HTML document
- Add inline Javascript
- Add Javascript in an external file

## 3. Rules to write Javascript
- Javascript is case-sensitive
- Use camelCase
- Whitespace Matters (to humans)
- End each statement with a semicolon
- Use comments liberally

# III. Working with Data
## 1. Variables
How to declare a variables:
``` 
var a;
var A;
var currentTime;
var unit_time;
var x = 4;
var sum = a + x;
```
## 2. Data Types
### Data Types in Javascript
- Numeric
- String
- Boolean
- null
- undefined
- Symbol (ES6)

``` 
var number = 1;  // Numeric
var floatNumber = 3.14; // Numeric
var str1 = "This is a string with double quote";  // String
var str2 = 'This is a string with single quote';  // String
var bool = fasle;  // Boolean
var emptyInside = null;  // null
var justAnotherVariable:  //undefined
```

## 3. Operators
- Assignment Operator: `=`
- Arithmetic Operators: `+`, `-`, `*`, `/` with Algebra Rules
  - Shorthand Math: `+=`, `-=`, `*=`, `/=`, `++`, `--`
  
## 4. Conditional Statement
### Conditional if Statement
``` 
if (some condition) {
  Do something.
} else {
  Do something else.
}
```

### Operators
- Equality Operators: `==`, `===`, `!=`, `!==`, `>=`, `<=`
- Logic Operators: `&&` (AND), `||` (OR)
- Ternary Operator:
```
a == b ? console.log('Match') : console.log('No match');
```

## 5. Arrays
### Declaration
``` 
var pens = ['red', 'green', 'blue'];
var pens = new Array('red', 'green', 'blue');
var emptyArray = [];
```

### Properties && Methods
``` 
var pens;
pens = ["red", "blue", "green", "orange"];

console.log("Before: ", pens);

// PROPERTIES:
// Get a property of an object by name:
// console.log("Array length: ", pens.length);

// METHODS:
// Reverse the array:
// pens.reverse();

// Remove the first value of the array:
// pens.shift();

// Add comma-separated list of values to the front of the array:
// pens.unshift("purple", "black");

// Remove the last value of the array:
// pens.pop();

// Add comma-separated list of values to the end of the array:
// pens.push("pink", "prussian blue");

// Find the specified position (pos) and remove n number of items from the array. Arguments: pens.splice(pos,n):
// pens.splice(pos, n) // Starts at the seccond item and removes two items.

// console.log("After: ", pens);

// Create a copy of an array. Typically assigned to a new variable:
// var newPens = pens.slice();
// console.log("New pens: ", newPens);

// Return the first element that matches the search parameter after the specified index position. Defaults to index position 0. Arguments: pens.indexOf(search, index):
// var result = pens.indexOf(search, index);
// console.log("The search result index is: ", result);

// Return the items in an array as a comma separated string. The separator argument can be used to change the comma to something else. Arguments: pens.join(separator):
// var arrayString = pens.join(separator);
// console.log("String from array: ", arrayString);
```

# IV. Functions & Objects
## 1. Functions in Javascript
Three types of functions:   
- Named functions  
- Anonymous functions  
- Immediately invoked function expressions (IIFE)  

## 2. Build a basic function
See the `examples/chap-4/basic-functions` directory.

## 3. Adding arguments to the function
See the `examples/chap-4/adding-arguments` directory.

## 4. Return a values from a function
See the `examples/chap-4/adding-arguments` directory.

## 5. Anonymous functions
See the `examples/chap-4/anonymous-functions` directory.

## 6. Immediately invoked functional expressions (IIFE)
See the `examples/chap-4/IIDE` directory.

## 7. Variable scopes
- Global
- Local (inside a function)

## 8. Const & let
### `const`  
- Constant
- Can't be changed once defined

### `let`  
- Block scope variable
- Smaller scope than var

## 9. Objects
See the `examples/chap-4/objects` directory.

## 10. Object constructors
See the `examples/chap-4/object-constructors` directory.

## 11. Dot and bracket notation
- Dot notation: `course.title`
- Bracket notation: `course["title"]`

# V. Javascript & The DOM 1
## 1. DOM: The document object model
- `window`: top level object
- `document`

## 2. Query selector methods
### DOM methods
- `getElementById`
- `getElementByClassName`
- `getElementByTagName`
- `querySelector`
- `querySelectorAll`

## 3. Access & change elements
See the `examples/chap-5/access-change-elements` directory.

## 4. Access & change classes
See the `examples/chap-5/access-change-classes` directory.

## 5. Access & change classes
See the `examples/chap-5/access-change-attributes` directory.

## 6. Add DOM Elements
## Adding new DOM elements
1. Create the element
2. Create the text does node that goes inside the element
3. Add the text node to the element
4. Add the element to the DOM tree

## DOM element creation methods
- `createElement()`: create an element
- `createTextNode()`: create text node
- `appendChild()`: place one child node inside another

## 7. Add inline CSS to an element
### Inline CSS
Add any CSS property to any element via Javascript using the style attribute

# VII. Javascript & The DOM 2
## 1. What is DOM events?
## 2. Typical DOM events
### Browser-level events
#### Browser behavior  
- Load - when the resource and dependents have finished loading
- Error - when a resource failed to load
- Online/offline - self-explanatory
- Resize - when a viewport is scrolled

### DOM-level events
#### Content interaction  
- Focus - when an element receives focus (clicked, tabbed to, etc.)
- Blur - when an element loses focus (leaving form field, etc.)
- Resets/submit - form-specific events
- Mouse events - click, mouseover, drag, drop, etc.

### Other events
- Media events - relate to audio/video playback
- Progress events - monitor ongoing browser progress
- CSS transition events - transition start/run/end

## 3. Trigger functions with event handlers
See the `examples/chap-7/trigger-events` directory.

## 4. Add & use event listeners
See the `examples/chap-7/event-listeners` directory.

## 5. Pass arguments via event listeners
See the `examples/chap-7/pass-arguments-event-listeners` directory.
# IX. Loops
## 1. Loops
For loop:
```javascript
for (var i = 0; i < 10; i++) {
  console.log(i);
}
```
While loop
```javascript
var i = 0;
while(i < 10) {
  console.log(i);
  i++;
}
```

Do-While loop
```javascript
var i = 0;
do {
  console.log(i);
  i++;
} while (i < 10);
```

## 2. Looping through arrays
See the `examples/chap-9/arrays-looping` directory.

## 3. Break and Continue loops
See the `examples/chap-9/break-continue-loops` directory.

# XI. Troubleshooting, Validating, & Minify Javascript
## 1. Javascript validation & troubleshooting
## 2. Troubleshooting Javascript
See the `examples/chap-11/troubleshooting-js` directory.

## 3. Send troubleshooting info to the console
See the `examples/chap-11/sending-troubleshooting-to-console` directory.

## 4. Step through your Javascript with browser tools
See the `examples/chap-11/browser-tools` directory.

## 5. Online script linting
- JSLint
- JSHint

## 6. Automate script linting
ESLint

## 7. Online script minification
minifier.org

## 8. Automate script minification
UglifyJS
Minifier