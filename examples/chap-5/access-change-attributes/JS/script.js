const CTA_ELEMENT = document.querySelector('.cta a');
if (CTA_ELEMENT.hasAttribute('target')) {
  console.log(CTA_ELEMENT.getAttribute('target'));
} else {
  CTA_ELEMENT.setAttribute('target', '_blank');
}

console.log(CTA_ELEMENT.attributes);