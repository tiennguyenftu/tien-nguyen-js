const CTA = document.querySelector(".cta a");
const ALERT = document.querySelector("#booking-alert");

CTA.classList.remove("hide");
ALERT.classList.add("hide");

function reveal(e, current) {
    e.preventDefault();

    CTA.innerHTML = current.innerHTML === 'Book Now!' ? 'Oooops!' : 'Book Now!';

      ALERT.classList.toggle("hide");
}

CTA.addEventListener('click', function(e) {
    reveal(e, this);
}, false);
CTA.addEventListener('click', function(){console.log("The button was clicked!")}, false);
