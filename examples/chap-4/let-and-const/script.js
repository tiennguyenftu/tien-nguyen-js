const MY_CONSTANT = 5;
console.log(MY_CONSTANT);

function logScope() {
  var localVar = 2;

  if (localVar) {
    let localVar = "I'm different";
    console.log('nested localVar', localVar);
  }

  console.log('logScope localVar: ', localVar);
}

logScope();