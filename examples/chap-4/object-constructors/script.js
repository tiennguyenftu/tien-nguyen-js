function Course(title, instructor, level, published, views) {
  this.title = title;
  this.instructor = instructor;
  this.level = level;
  this.published = published;
  this.views = views;
  this.updateViews = function () {
    return ++this.views;
  }
}

var courses = [
  new Course('Javascript Essential Training', 'Morten Rand-Hendriksen', 0, true, 0),
  new Course('Up and Running with ECMASScript 6', 'Morten Danilo', 1, true, 12)
];

console.log(courses[1].instructor);